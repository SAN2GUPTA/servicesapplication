package com.san.testapplication;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public final class CustomCompoundViewRender extends LinearLayout {
    private LinearLayout linearClick;
    private TextView txt_Emergency_Line_01, txt_Open27X7_01, txt_phone_01;

    public CustomCompoundViewRender(Context context) {
        super(context);
        init();
    }

    public CustomCompoundViewRender(Context context, AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    public CustomCompoundViewRender(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        init();
    }

    private void init() {
        setOrientation(LinearLayout.VERTICAL);

        View childView = inflate(getContext(), R.layout.item_dynamic_row, this);

        linearClick = childView.findViewById(R.id.linearClick);
        txt_Emergency_Line_01 = childView.findViewById(R.id.txt_Emergency_Line_01);
        txt_Open27X7_01 = childView.findViewById(R.id.txt_Open27X7_01);
        txt_phone_01 = childView.findViewById(R.id.txt_phone_01);

        //this.addView(childView);
        //setDataInfo();
    }

    public void setDataInfo(final Integer i) {
        txt_Emergency_Line_01.setText("Emergency line"+i);
        txt_Open27X7_01.setText("09:00 - 21:00"+i);
        txt_phone_01.setText("+31 22 99 992"+i);


        /*linearClick.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), ""+i, Toast.LENGTH_SHORT).show();
            }
        });*/

    }
}
