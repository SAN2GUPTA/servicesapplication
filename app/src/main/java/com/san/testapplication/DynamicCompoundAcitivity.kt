package com.san.testapplication

import android.content.Context
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class DynamicCompoundAcitivity : AppCompatActivity() {

    private var linearParent: LinearLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dynamic_compound_acitivity)

        linearParent = findViewById(R.id.linearParent)


        for (i in 0..9) {
            val customCompoundViewRender = CustomCompoundViewRender(baseContext)
            linearParent?.addView(customCompoundViewRender)

            customCompoundViewRender.setOnClickListener{
                Toast.makeText(baseContext, ""+i, Toast.LENGTH_SHORT).show();
            }
            customCompoundViewRender.setDataInfo(i)
        }
    }
}
