package com.san.testapplication

import android.app.ActionBar
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.content.Context
import android.content.Context.LAYOUT_INFLATER_SERVICE
import androidx.core.content.ContextCompat.getSystemService
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.item_row_01.view.*


class DynamicVeiwActivity : AppCompatActivity() {

    private var linearMain: LinearLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dynamic_veiw)

        linearMain = findViewById(R.id.linearMain)
        var childView: View
        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        for (i in 0..9) {
            childView = inflater.inflate(R.layout.item_dynamic_row, null)

            childView.txt_Emergency_Line_01.text  = "Emergency line $i"
            childView.txt_Open27X7_01.text = "09:00 - 21:00 $i"
            childView.txt_phone_01.text = "+31 22 99 992 $i"

            childView.id = i
            childView.setOnClickListener{
                Toast.makeText(this, "Clicked $i", Toast.LENGTH_SHORT).show()
            }
            linearMain?.addView(childView)
        }

        /*var params = LinearLayout.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT)
        val phonenumbers = TextView(this)
        phonenumbers.text = "Phone numbers"
        phonenumbers.textSize = 20f
        phonenumbers.setTextColor(Color.BLUE)
        phonenumbers.setPadding(16,10, 16, 10)
        linearMain?.addView(phonenumbers)
        for (i in 0..2) {
            val ll = LinearLayout(this)
            ll.orientation = LinearLayout.HORIZONTAL

            val product = TextView(this)
            product.text = "Emergency Line"
            product.textSize = 20f
            product.setTextColor(Color.BLUE)
            ll.addView(product)

            ll.setLayoutParams(params);
            linearMain?.addView(ll);
        }*/


        // for dynamic view urls
        // https@ //github.com/uddish/DynamicLayouts/tree/master/app/src/main/java/com/example/uddishverma/dynamiclayoutsexample
        // https@ //androidexample.com//index.php?view=article_discription&aid=115&aaid=

    }
}
