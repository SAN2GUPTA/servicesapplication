package com.san.testapplication

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_row_01.*
import kotlinx.android.synthetic.main.item_row_02.*
import kotlinx.android.synthetic.main.item_row_03.*
import kotlinx.android.synthetic.main.item_row_04.*
import kotlinx.android.synthetic.main.item_row_05.*
import kotlinx.android.synthetic.main.item_row_06.*
import kotlinx.android.synthetic.main.item_row_07.*
import kotlinx.android.synthetic.main.item_row_08.*
import kotlinx.android.synthetic.main.item_row_09.*
import kotlinx.android.synthetic.main.item_row_10.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initToolbar()
        setTextValue()

    }

    private fun initToolbar() {
        toolbar.title = toolbar.context.getString(R.string.app_name)
        toolbar.setTitleTextColor(Color.DKGRAY)
        toolbar?.inflateMenu(R.menu.menu_home)
        toolbar?.setOnMenuItemClickListener{
            when(it.itemId){
                R.id.menu_user_settings ->{
                    val intent = Intent(this, DynamicVeiwActivity::class.java)
                    startActivity(intent)
                    true
                }
                else -> false
            }
        }
        toolbar.setOnClickListener{
            val intent = Intent(this, DynamicCompoundAcitivity::class.java)
            startActivity(intent)
        }
    }

    private fun setTextValue() {
        setText_01()
        setText_02()
        setText_03()
        setText_04()
        setText_05()
        setText_06()
        setText_07()
        setText_08()
        setText_09()
        setText_10()
    }

    private fun setText_01(){
        txt_Emergency_Line_01.text = txt_Emergency_Line_01.context.getString(R.string.emergency_line);
        txt_Open27X7_01.text = txt_Open27X7_01.context.getString(R.string.open_24X7)
        txt_phone_01.text = "+31 20 22 888 00"
    }
    private fun setText_02(){
        txt_Emergency_Line_02.text = txt_Emergency_Line_02.context.getString(R.string.customer_service);
        txt_Open27X7_02.text = "08:00 - 21:00"
        txt_phone_02.text = "+31 20 22 888 00"
    }
    private fun setText_03(){
        txt_Emergency_Line_03.text = txt_Emergency_Line_03.context.getString(R.string.report_damage);
        txt_Open27X7_03.text = "08:00 - 22:00"
        txt_phone_03.text = "+31 20 22 888 33"
    }
    private fun setText_04(){
        txt_Emergency_Line_04.text = txt_Emergency_Line_04.context.getString(R.string.report_damage);
        txt_Open27X7_04.text = "08:00 - 21:00"
        txt_phone_04.text = "+31 20 22 888 44"
    }
    private fun setText_05(){
        txt_Emergency_Line_05.text = txt_Emergency_Line_05.context.getString(R.string.report_damage);
        txt_Open27X7_05.text = "08:00 - 21:00"
        txt_phone_05.text = "+31 20 22 888 55"
    }
    private fun setText_06(){
        txt_Emergency_Line_06.text = txt_Emergency_Line_06.context.getString(R.string.report_damage);
        txt_Open27X7_06.text = "08:00 - 21:00"
        txt_phone_06.text = "+31 20 22 888 66"
    }
    private fun setText_07(){
        txt_Emergency_Line_07.text = txt_Emergency_Line_07.context.getString(R.string.report_damage);
        txt_Open27X7_07.text = "08:00 - 21:00"
        txt_phone_07.text = "+31 20 22 888 77"
    }
    private fun setText_08(){
        txt_Emergency_Line_08.text = txt_Emergency_Line_08.context.getString(R.string.report_damage);
        txt_Open27X7_08.text = "08:00 - 21:00"
        txt_phone_08.text = "+31 20 22 888 88"
    }
    private fun setText_09(){
        txt_Emergency_Line_09.text = txt_Emergency_Line_09.context.getString(R.string.report_damage);
        txt_Open27X7_09.text = "08:00 - 21:00"
        txt_phone_09.text = "+31 20 22 888 99"
    }
    private fun setText_10(){
        txt_Emergency_Line_10.text = txt_Emergency_Line_10.context.getString(R.string.report_damage);
        txt_Open27X7_10.text = "08:00 - 21:00"
        txt_phone_10.text = "+31 20 22 888 10"
    }
}
